---
title: "The Art of Computer Programming: Exercises Solutions"
---

# Volume 1

## Notes on the Exercises

1. Dificuldade média, exercício de matemática;
2. Dão a oportunidade de pensar a respeito do que foi lido e testar a
   compreensão do assunto;
3. Prove that $`13^3 = 2197`$. Generalize your answer.

Pela definição da potenciação:

```math
13^3 = \underbrace{13 \times 13 \times 13}_{3 \text{ vezes}} = 2197
```

Observamos também que:

```math
\begin{align*}
    13^2 & = 13 \times 13 \\
    13^1 & = 13 \\
    & \therefore \\
    13^3 & = 13^2 \times 13
\end{align*}
```

Generalizando:

```math
\begin{cases}
    b^1 = b \\
    b^{n+1} = b^n \times b, \forall n \in \mathbb{N^+}
\end{cases}
```

Não sei se é necessário. Mas, para provar que a generalização está correta
precisaria demonstrar que $`b^1 = b`$ é verdadeiro e que $`b^{n+1} = b^n \times b`$
é verdadeiro para $`n = k`$ e $`n = k + 1`$ para qualquer $`k \in \mathbb{N^+}`$.

4. Este é o Último Teorema de Fermat, também conhecido como Teorema de
   Fermat-Wiles. A solução envolve as seguintes técnicas da matemática, que eu
   não conheço:
  * Curvas Elípticas;
  * Formas Modulares;
  * Representações Galoisianas;
  * Conjectura de Shimura-Taniyama-Weil.

